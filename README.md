# 分享文档
[【分享】TestToolbox | 测试工具箱](https://kqnehw82cb.feishu.cn/docs/doccnBvx4YVeiO6YesdKv6w2BnG)

# 里程碑 需求池
1. 工具 1.0 基础功能准备
*  [x] 解析logman
*  [x] 格式化数据
*  [x] 快速查找数据
2. 接口网络数据解析
*  [ ] 请求头转换成Json
*  [x] 请求Param 转换 Json
*  [ ] 请求Query 转换 Json
*  [ ] Curl 转换
*  [ ] .har 文件转换
3. 网络代理抓包
*  [ ] 埋点解析, 事件上报比对
*  [ ] 接口mock工具


# testToolBox测试工具箱演示

- 食用姿势:
```
# 环境依赖:
# pip install jsonpath
# pip install yaml

# 同级目录新建 tool.bat 文件, 写入以下内容 

start python .\TestToolbox.py
```

### 相关背景

- 需求驱动
- 组合拼装
- 经验方法沉淀

- 日常工作需求
- 自动化测试总结沉淀
- 测试流程优化
