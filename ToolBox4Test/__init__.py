#!/usr/bin/python
# -*- coding: UTF-8 -*- 
"""   
@Author    : Gavin
@DateTime  : 2021/6/9 09:40
@Contact   : wen.guo@foxmail.com

@Project   : ToolBox4Test
@File      : __init__.py.py
@Describe  : TestToolbox 测试工具箱 | 辅助快速测试与验证
————————————————————
@Version   : 0.1 
"""
# update   : 
# 1. 
# 2. 
# ————————————————————
# update   : 
# 1. 
# 2. 
# ————————————————————

import os
import sys


_project_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(_project_dir)

